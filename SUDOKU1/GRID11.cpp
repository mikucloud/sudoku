#include<iostream>
#include<vector>
using namespace std;

int InitialGrid[9][9] = {
	{ 0,5,0,3,6,0,8,4,0 },
	{ 8,0,0,0,0,0,2,7,0 },
	{ 0,0,0,0,7,0,6,0,0 },
	{ 0,2,0,5,0,4,0,0,0 },
	{ 0,0,6,8,0,7,9,0,0 },
	{ 0,0,0,6,0,9,0,3,0 },
	{ 0,0,2,0,5,0,0,0,0 },
	{ 0,8,7,0,0,0,0,0,6 },
	{ 0,3,5,0,8,1,0,2,0 },
};

int grid[9][9];

int PosValGrid[9][9];

int c;
int d;
bool JePoleSJednouMoznosti=false;

bool CheckGrid();
void FillGrid();
void RefillGrid();
int PrintGrid();
int PrintPosValGrid();
bool PossibleValues();
bool CheckRaw(int i, int j, int n);
bool CheckColumn(int i, int j, int n);
bool CheckSquare(int i, int j, int n);
int VratiC(int i);
int VratiD(int j);



bool CheckGrid()
{
	bool Finished = false;
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (grid[i][j] == 0)
			{
				Finished = true;
				goto konecCheckGrid;
			}		
		}
	}
	konecCheckGrid:
	return Finished;
}



void FillGrid()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			grid[i][j] = InitialGrid[i][j];
		}
	}
}

void RefillGrid()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (!PosValGrid[i][j]==0)
			grid[i][j] = PosValGrid[i][j];
		}
	}
}

int PrintGrid()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			cout << grid[i][j];
		}
		cout << "\n";
	}
	return 0;
}

int PrintPosValGrid()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			cout << PosValGrid[i][j];
		}
		cout << "\n";
	}
	return 0;
}

bool PossibleValues()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (!grid[i][j] == 0)
				continue;
			//cout << "position" << i << j << "is checked. The value is" << grid[i][j] << "\n";
			vector<int> PosVal;

			for (int n = 1; n < 10; n++)

			{

				if (!(CheckRaw(i, j, n) || CheckColumn(i, j, n) || CheckSquare(i, j, n)))
					//if (CheckSquare(i, j, n))
					//cout << n << " nemuze byt\n";
					//else
					//cout << n << " muze byt\n";
					PosVal.push_back(n);

			}
			//cout << PosVal.size() << "\n";
			if (PosVal.size() == 1)
			{
				//cout << PosVal[0]<<"\n";
				PosValGrid[i][j] = PosVal[0];
				JePoleSJednouMoznosti = true;
			}
		}
	}
	return JePoleSJednouMoznosti;
}


int VratiC(int i)
{
	if (i >= 0 && i < 3)
		c = 0;
	else
	{
		if (i >= 3 && i < 6)
			c = 3;
		else
			c = 6;
	}
	return c;
}

int VratiD(int j)
{
	if (j >= 0 && j < 3)
		d = 0;
	else
	{
		if (j >= 3 && j < 6)
			d = 3;
		else
			d = 6;
	}
	return d;
}


bool CheckRaw(int i, int j, int n)
{
	int b;
	bool jeTam;
	for (b = 0; b < 9; b++)
	{

		if (b == j)
			continue;

		if (grid[i][b] == n)
		{
			//cout << n << "R je na pozici" << i<<b<<"\n";
			jeTam = true;
			break;
		}

		else
		{
			//cout << n << "R tam neni" << "\n";
			jeTam = false;
		}

		//cout << n << "R neni tam" << "\n";

	}
	return jeTam;
}


bool CheckColumn(int i, int j, int n)
{
	int b;
	bool jeTam;
	for (b = 0; b < 9; b++)
	{
		if (b == i)
			continue;
		{
			if (grid[b][j] == n)
			{
				//cout << n << "C je tam" << "\n";
				jeTam = true;
				break;
			}
			else
			{
				jeTam = false;
			}
			//cout << n << "C neni tam" << "\n";
		}
	}
	return jeTam;
}


bool CheckSquare(int i, int j, int n)
{

	//int k;
	//int l;
	bool jeTam;

	//cout << "position" << i << j << "is checked. The value is" << grid[i][j] << "\n";
	for (int k = VratiC(i); k < (VratiC(i) + 3); k++)
	{


		for (int l = VratiD(j); l < (VratiD(j) + 3); l++)
		{

			if ((k == i) && (l == j))
				continue;


			if (grid[k][l] == n)
			{
				//cout << n << "je tam\n";
				jeTam = true;
				goto konec;

			}

			else
				jeTam = false;
			//cout << grid[k][l] << "\n";
		}


	}
konec:
	//cout << "konec";
	return jeTam;

}

int main()
{
	FillGrid();
	int iterace = 1;
	do
	{
		
		//CheckRaw(0,3,4);
		//CheckColumn(0,3,4);
		//CheckSquare(0,3,9);
		JePoleSJednouMoznosti = false;
		PossibleValues();
		RefillGrid();
		cout<<"iterace cislo:"<<iterace<<"\n";
		iterace++;

		if (JePoleSJednouMoznosti == true)
		{
			PrintGrid();
			cout << "\n";
			PrintPosValGrid();
			cout << "\n";
			CheckGrid();
		}
	} while (CheckGrid() == true && JePoleSJednouMoznosti == true);
	
		


	if (JePoleSJednouMoznosti==false)
		{
			cout << "Minimalne 2 moznosti";
		}
	
			
	cin.get();
	return 0;
}